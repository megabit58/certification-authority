#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QTcpSocket>
#include <QtWidgets>
#include <ui_mainwindow.h>

#include "x509v3cert.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
    QTcpSocket* m_pTcpSocket;
    quint16     m_nNextBlockSize;
    QLabel *status_Label;
    QString host;
    int port;
    bool readyReadData = false;

    void nameToArray(QByteArray &, QMap<QByteArray, QByteArray> &);
    int readFile(QString &, QByteArray &);
    int writeFile(QString &nameWindows, QByteArray &, QString &mask);
    QByteArray readData(); // получение данных из сокета
    int sendToServer(QByteArray & arrBlock); // отправка данных серверу
    void connectToServ();


private slots:
    void slotError       (QAbstractSocket::SocketError);
    void slotReadyRead   ();
    void slotConnected();
    void slotDisconnected();
    void on_getCert_Button_clicked();
    void on_cert_Button_clicked();
    void on_checkCert_Button_clicked();
    void on_action_clicked();
};

#endif // MAINWINDOW_H
