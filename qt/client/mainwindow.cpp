#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_nNextBlockSize(0)
{
    ui->setupUi(this);

    status_Label = new QLabel(this);
    statusBar()->addWidget(status_Label);
    status_Label->setText("Соединение с сервером отсутствует");
    ui->table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    m_pTcpSocket = new QTcpSocket(this);

    host = "127.0.0.1";
    port = 2323;

    connectToServ();
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(disconnected()), SLOT(slotDisconnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,         SLOT(slotError(QAbstractSocket::SocketError))
            );


    ui->menudf->addAction("Адрес сервера", this, SLOT(on_action_clicked()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectToServ()
{
    m_pTcpSocket->connectToHost(host, port);
}

void MainWindow::slotReadyRead()
{
    readyReadData = true;
}

QByteArray MainWindow::readData()
{
    QDataStream in(m_pTcpSocket);
    in.setVersion(QDataStream::Qt_4_2);
    QByteArray data;
    for (;;) {
        if (!m_nNextBlockSize) {
            if (m_pTcpSocket->bytesAvailable() < sizeof(quint16)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (m_pTcpSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }
        m_nNextBlockSize = 0;
        in >> data;
    }
    readyReadData = false;
    return data;
}


void MainWindow::slotError(QAbstractSocket::SocketError err)
{
    QString strError =
        "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(m_pTcpSocket->errorString())
                    );
    //m_ptxtInfo->append(strError);
}


int MainWindow::sendToServer(QByteArray & data)
{
    QByteArray  arrBlock;

    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_2);
    out << quint16(0) << data;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    if(m_pTcpSocket->state() != QAbstractSocket::ConnectedState)
    {
        connectToServ();
        if(!m_pTcpSocket->waitForConnected(1000))
        {
            QMessageBox::critical(0, "Ошибка", "Нет соединения с сервером", "OK", QString(), 0, 1);
            return 1;
        }
    }

    m_pTcpSocket->write(arrBlock);

    QProgressDialog* dialogWait = new QProgressDialog("Ожидание ответа сервера...", "&Cancel", 0, 0);
    dialogWait->setMinimumDuration(0);
    dialogWait->setWindowTitle("Пожалуйста, подождите");
    dialogWait->setModal(true);
    while(1)
    {
        dialogWait->setValue(0) ;
        qApp->processEvents();
        if ((dialogWait->wasCanceled()) || (readyReadData == true))
             break;
    }
    dialogWait->setValue(0) ;
    delete dialogWait;
    return 0;
}

void MainWindow::slotConnected()
{
    status_Label->setText("Соединие с сервером установлено");
}

void MainWindow::slotDisconnected()
{
    status_Label->setText("Соединение с сервером отсутствует");
}

int MainWindow::readFile(QString &name, QByteArray &data)
{
    QFile file(name);
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(0, "Ошибка", "Ошибка чтения файла", "OK", QString(), 0, 1);
        return 1;
    }
    data += file.readAll();

    file.close();
    return 0;
}

int MainWindow::writeFile(QString &nameWindows, QByteArray &data, QString &mask)
{
    QString name = QFileDialog::getSaveFileName(this,
            tr(nameWindows.toUtf8()), "", tr(mask.toUtf8()));
    if (name == "")
        return 1;
    QFile file(name);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QMessageBox::warning(0, "Ошибка", "Ошибка записи файла", "OK", QString(), 0, 1);
        return 1;
    }
    QDataStream outf(&file);
    for(int i = 0; i < data.size(); i++)
        outf << (unsigned char)data[i];
    if(outf.status() != QDataStream::Ok)
    {
        QMessageBox::warning(0, "Ошибка", "Ошибка записи файла", "OK", QString(), 0, 1);
        return 1;
    }
    file.close();
    return 0;
}

void MainWindow::nameToArray(QByteArray &arr, QMap<QByteArray, QByteArray> &m)
{
    X509V3Cert x509;
    arr += x509.X509NameToDer(m);

}

void MainWindow::on_getCert_Button_clicked()
{

    if ((ui->C_lineEdit->text() == "") || (ui->ST_lineEdit->text() == "") || (ui->L_lineEdit->text() == "")
             || (ui->O_lineEdit->text() == "") || (ui->OU_lineEdit->text() == "") || (ui->CN_lineEdit->text() == ""))
    {
        QMessageBox::warning(0, "Ошибка", "Заполните все поля", "OK", QString(), 0, 1);
        return;
    }

    QByteArray data;
    data.push_back(1);
    data.push_back(ui->period_spinBox->value());


    QMap<QByteArray, QByteArray> name;
    name["C"] = ui->C_lineEdit->text().toUtf8();
    name["ST"] = ui->ST_lineEdit->text().toUtf8();
    name["L"] = ui->L_lineEdit->text().toUtf8();
    name["O"] = ui->O_lineEdit->text().toUtf8();
    name["OU"] = ui->OU_lineEdit->text().toUtf8();
    name["CN"] = ui->CN_lineEdit->text().toUtf8();

    nameToArray(data, name);


    if (sendToServer(data) != 0)
        return;

    data = readData();
    if (data == "success")
    {
        QMessageBox::information(0, " ", "Запрос на сертификат открытого ключа отправлен", "OK", QString(), 0, 1);
        return;
    }
    else
    {
        QMessageBox::critical(0, "Ошибка", "Ошибка при передаче данных", "OK", QString(), 0, 1);
        return;
    }

    //if (data.size() == 0)
    //    return;
    //writeFile((QString)"Сохранение сертификата", data, (QString)"Certificate Files (*.crt)");
}

void MainWindow::on_cert_Button_clicked()
{
    ui->cert_lineEdit->setText(QFileDialog::getOpenFileName(this,
        tr("Выберите файл цифрового сертификата"), "", tr("Certificate Files (*.der)")));

    if (ui->cert_lineEdit->text() == "") return;
    QByteArray data;
    //data.push_back(2);
    if(readFile(ui->cert_lineEdit->text(), data) != 0)
        return;
    X509V3Cert certClient;
    if (certClient.openCert(data) != 0)
    {
        QMessageBox::warning(0, "Ошибка", "Выберите файл сертификата для проверки", "OK", QString(), 0, 1);
        return;
    }

    // Версия Серийный номер Алгоритм подписи Хэш-алгоритм подписи Издатель Действителен с Действителен по Субъект Открытый ключ

    ui->table->setRowCount(9);
    for(int row = 0; row < ui->table->rowCount(); row++)
      for(int column = 0; column < ui->table->columnCount(); column++)
          ui->table->setItem(row, column, new QTableWidgetItem());

    ui->table->item(0, 0)->setText("Версия");
    ui->table->item(0, 1)->setText("V2");
    ui->table->item(1, 0)->setText("Серийный номер");
    ui->table->item(1, 1)->setText(QString::number(certClient.SerialNum, 16));
    ui->table->item(2, 0)->setText("Алгоритм подписи");
    ui->table->item(2, 1)->setText(certClient.SignatureAlgorithm);
    ui->table->item(3, 0)->setText("Хэш-алгоритм подписи");
    ui->table->item(3, 1)->setText(certClient.HashAlgorithm);
    ui->table->item(4, 0)->setText("Издатель");
    QString tmp;
    QMapIterator<QByteArray, QByteArray> iter(certClient.IssuerDN);
    while (1)
    {
        iter.next();
        tmp += iter.key() + "=";
        tmp += iter.value();
        if (iter.hasNext())
            tmp += ", ";
        else break;
    }
    ui->table->item(4, 1)->setText(tmp);
    ui->table->item(5, 0)->setText("Действителен с");
    ui->table->item(5, 1)->setText(certClient.NotBefore.toString("dd.MM.yyyy hh:mm:ss"));
    ui->table->item(6, 0)->setText("Действителен по");
    ui->table->item(6, 1)->setText(certClient.NotAfter.toString("dd.MM.yyyy hh:mm:ss"));
    ui->table->item(7, 0)->setText("Субъект");
    tmp = "";
    QMapIterator<QByteArray, QByteArray> iter2(certClient.SubjectDN);
    while (1)
    {
        iter2.next();
        tmp += iter2.key() + "=";
        tmp += iter2.value();
        if (iter2.hasNext())
            tmp += ", ";
        else break;
    }
    ui->table->item(7, 1)->setText(tmp);
    ui->table->item(8, 0)->setText("Открытый ключ");
    tmp = "";
    for(int i = 0; i < certClient.PubKey.H.length(); i++)
        tmp += QString::number(certClient.PubKey.H[i], 16);
    tmp += '\n';
    tmp += QString::number(certClient.PubKey.t, 16);
    ui->table->item(8, 1)->setText(tmp);

    ui->checkCert_Button->setEnabled(true);
}

void MainWindow::on_checkCert_Button_clicked()
{
    if (ui->cert_lineEdit->text() == "")
    {
        QMessageBox::warning(0, "Ошибка", "Выберите файл сертификата для проверки", "OK", QString(), 0, 1);
        return;
    }
    QByteArray data;
    data.push_back(2);
    if(readFile(ui->cert_lineEdit->text(), data) != 0)
        return;

    if (sendToServer(data) != 0)
        return;

    data = readData();
    if (data.size() == 0)
        return;
    if (data == "err")
    {
        QMessageBox::warning(0, "Ошибка", "Целостность сертификата не может быть проверена", "OK", QString(), 0, 1);
        return;
    }
    if (data == "valid")
    {
        QMessageBox::information(0, "Сертификат действителен", "Сертификат действителен", "OK", QString(), 0, 1);
        return;
    }
    if (data == "expired")
    {
        QMessageBox::warning(0, "Сертификат недействителен", "Срок дейтсвия сертификата истек или еще не наступил", "OK", QString(), 0, 1);
        return;
    }
    if (data == "notValid")
    {
        QMessageBox::warning(0, "Сертификат недействителен", "Целостность сертификата нарушена", "OK", QString(), 0, 1);
        return;
    }
    if (data == "revoked")
    {
        QMessageBox::warning(0, "Сертификат недействителен", "Сертификат аннулирован удостоверяющим центром", "OK", QString(), 0, 1);
        return;
    }

}

void MainWindow::on_action_clicked()
{
    bool bOk;
    QString str = QInputDialog::getText( 0,
                                         "Настройки",
                                         "IP-адрес и номер порта сервера:",
                                         QLineEdit::Normal,
                                         host.append(":%1").arg(port),
                                         &bOk
                                        );
    if (bOk) {
        // Была нажата кнопка Cancel
    }
}
