#ifndef X509V3CERT_H
#define X509V3CERT_H
#include "structKey.h"
#include <QtCore>



class X509V3Cert
{

public:
    unsigned __int32    SerialNum;          // серийный номер
    PublicKey   PubKey;               // открытый ключ
    QMap<QByteArray, QByteArray>
        SubjectDN,         // данные о субъекте
        IssuerDN;          // данные о выпускающем

    QByteArray
        SignatureAlgorithm = "",// алгоритм подписи
        HashAlgorithm = "";         // подпись
    QDateTime
        NotAfter,           // недействителен после
        NotBefore;          // недействителен до

private:
    const QMap<QByteArray, unsigned __int32> X509NameConst = {
        { "C",  0x550406 },
        { "ST", 0x550408 },
        { "L",  0x550407 },
        { "O",  0x55040A },
        { "OU", 0x55040B },
        { "CN", 0x550403 }
    };
    const QMap<unsigned __int32, QByteArray> revX509NameConst = {
        { 0x550406, "C" },
        { 0x550408, "ST"},
        { 0x550407, "L"},
        { 0x55040A, "O"},
        { 0x55040B, "OU"},
        { 0x550403, "CN"}
    };

public: // спецификатор доступа public
    //int genTSB();
    //QByteArray getTSB(PublicKey &);
    //QByteArray getCert(QByteArray &);
    QByteArray X509NameToDer(QMap<QByteArray, QByteArray> &Name);
    QByteArray procReq(QByteArray &, QString &); // обработка запроса на выдачу сертификата
    int getSelfSignedCert();
    QByteArray KeyToByte(PublicKey key);
    QByteArray KeyToByte(PrivateKey key);
    int openCert(QByteArray &);

private:
    void addSignInfo(QByteArray &vec);



    unsigned char getSizeInt(unsigned __int32); // узнать количество занятых байт числом
    void sizeToDer(QByteArray &, unsigned __int32); // записать размер в вектор
    void intToDer(QByteArray &, unsigned __int32); // записать число в вектор
    void ByteToByte(QByteArray &, unsigned __int32, unsigned char);
    //int sizeDecoder(QByteArray &);
    QByteArray getCert(QByteArray &, QByteArray &, int);
    QByteArray genTSB(int, QByteArray &, QByteArray &, QByteArray &, int);
    int keyToFile(int, QByteArray &, QByteArray &);

    QByteArray getData(QByteArray &);
    QMap<QByteArray, QByteArray> DerToX509Name(QByteArray &name);
    unsigned __int32 derToInt(QByteArray &arr);
    PublicKey X509V3Cert::pubKeyDecoder(QByteArray &key);
    unsigned int sizeDecoder(QByteArray &arr);
};
#endif // X509V3CERT_H
