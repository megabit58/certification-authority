#ifndef NIEDERCRYPT_H
#define NIEDERCRYPT_H
#include "structKey.h"
//#include <thread>
#include <QtCore>
#include <ctime>
#include <algorithm>
#include <functional>


#define hashSize 256 // длина хэш-суммы в битах
#define Algorithm QCryptographicHash::Sha224
//#define p_x 0x12b // неприводимый многочлен
#define p_x 0x12b // неприводимый многочлен

class NiederCrypt
{
private:
    unsigned __int16
        st = 0, // степень многочлена
        Q, // длина
        N, // длина блока
        K, // количество информационных символов, n - k = hashSize
        d_min, // минимальное кодовое расстояние
        t; // количство ошибок, которое исправляет
    unsigned char *alpha_of, *index_of;
    QVector<QVector<unsigned char>>
        Hpub,
        S,
        H,
        P;

public:
    NiederCrypt(); // конструктор класса
    void genKey(unsigned char, unsigned char); // генерация ключа
    QByteArray signature(QByteArray &); // подпись
    bool checkSign(QByteArray &, QByteArray &); // проверка подписи
    PublicKey getPubKey();
    PrivateKey getPrivKey();
    void setPubKey(PublicKey &);
    void setPrivKey(PrivateKey &);

private:
    void gen_table(); // генерация таблицы быстрого умножения в поле Галуа
    unsigned char gf_mul(__int16, __int16); // умножение в поле Галуа
    unsigned char gf_div(__int16, __int16); // деление в поле Галуа
    unsigned char gf_pow(unsigned char, __int16); // Возведение в степень в поле Галуа
    QVector<unsigned char> vec_mul(QVector<unsigned char>&, QVector<unsigned char>&); // умножение полиномов в поле Галуа
    QVector<QVector<unsigned char>> matrix_Transp(QVector<QVector<unsigned char>>&); // Транспонирование матрицы
    QVector<QVector<unsigned char>> matrix_mul(QVector<QVector<unsigned char>> &, QVector<QVector<unsigned char>> &); // умножение матриц в поле Галуа
    QVector<unsigned char> matrix_mul(QVector<unsigned char> &, QVector<QVector<unsigned char>> &); // умножение вектора на матрицу в поле Галуа
    QVector<unsigned char> matrix_mul(QVector<QVector<unsigned char>> &, QVector<unsigned char> &); // умножение матрицы на вектор в поле Галуа
    QVector<unsigned char> matrix_mul(QVector<QVector<unsigned char>> &a, QByteArray &b);
    QVector<QVector<unsigned char>> Get_matr(QVector<QVector<unsigned char>> &, int, int); //функция вычеркивания строки и столбца
    unsigned char Det(QVector<QVector<unsigned char>> &); // нахождение определителя
    QVector<QVector<unsigned char>> Obr(QVector<QVector<unsigned char>> &); // нахождение обратной матрицы
    void gen_S(); // генерация матрицы S
    void gen_P(); // генерация матрицы перестановки P
    unsigned char f_x(QVector<unsigned char> &, unsigned char); // нахождение значения функции f(x)
    QVector<unsigned char> decoder(QByteArray &); // декодер
    QByteArray hash(QByteArray &); // SHA-256
    struct rezDecod {
        unsigned i;
        QByteArray syndr_i;
        QVector<unsigned char> Z;
    };
    QVector<unsigned char> callDecoder (QByteArray &, unsigned);
};
#endif // NIEDERCRYPT_H
