#include "TcpServer.h"
#include <QtCore>
#include <QTextCodec>
#include <iostream>

#include <QTextStream>
QTextStream cout(stdout);
QTextStream cin(stdin);

TcpServer::TcpServer(QObject* pwgt /*=0*/) :
    QObject(pwgt), m_nNextBlockSize(0)
{

    ;
}

int TcpServer::startServer()
{

    if (!createConnectionBD())
    {
        qDebug() << "Cannot open database:";
        return 1;
    }

     // проверка наличия корневого сертификата
    if ((!QFile(SelfSignedCertName).exists()) || (!QFile(KeySelfSignedCertName).exists()))
    {
        X509V3Cert CA;
        print("Введите сведения об удостоверяющем центре:\n");
        do
        {
            print(" - двухсимвольный код страны: ");
            CA.IssuerDN["C"] = cin.readLine().toUtf8();
        }
        while(CA.IssuerDN["C"].length() != 2);

        print(" - регион: ");
        CA.IssuerDN["ST"] = cin.readLine().toUtf8();
        print(" - населенный пункт: ");
        CA.IssuerDN["L"] = cin.readLine().toUtf8();
        print(" - наименование организации: ");
        CA.IssuerDN["O"] = cin.readLine().toUtf8();
        print(" - наименование подразделения организации: ");
        CA.IssuerDN["OU"] = cin.readLine().toUtf8();
        print(" - общее название УЦ: ");
        CA.IssuerDN["CN"] = cin.readLine().toUtf8();


        NiederCrypt crypt;
        print("Размер ключа для самозаверенного сертификата:\nn = ");
        int n, k;
        cin >> n;
        print("k = ");
        cin >> k;
        crypt.genKey(n, k); // 20 16
        QByteArray privKey = CA.KeyToByte(crypt.getPrivKey());
        saveToFile(KeySelfSignedCertName, privKey);

        saveToFile(SelfSignedCertName, CA.getSelfSignedCert(CA.KeyToByte(crypt.getPubKey()), privKey));

        QSqlQuery query;
        QString   str  = "CREATE TABLE issuedCertificates ( "
                             "serialNo INTEGER PRIMARY KEY NOT NULL, "
                             "subjectDN   TEXT NOT NULL, "
                             "NotAfter  TEXT NOT NULL, "
                             "NotBefore  TEXT NOT NULL ); ";


        if (!query.exec(str))
        {
            qDebug() << "Unable to create a table issuedCertificates";
            return 1;
        }
        str = "CREATE TABLE revokedCertificates ( "
             "serialNo INTEGER PRIMARY KEY NOT NULL, "
             "date TEXT NOT NULL, "
             "comment TEXT NOT NULL ); ";
        if (!query.exec(str))
        {
            qDebug() << "Unable to create a table revokedCertificates";
            return 1;
        }

    }

    if (cert.openCert(openFile(SelfSignedCertName)) != 0)
    {
        print("Файл "+SelfSignedCertName.append(" не является сертификатом открытого ключа"));
        return 1;
    }

    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, nPort))
    {
        qDebug() << "Unable to start the server:" << m_ptcpServer->errorString() <<"\n";
        m_ptcpServer->close();
        return 1;
    }

    print("\n*** Сервер запущен ***\n");
    connect(m_ptcpServer, SIGNAL(newConnection()),
            this,         SLOT(slotNewConnection())
           );
    QFuture<void> future = QtConcurrent::run(this, &TcpServer::readCommand);
    return 0;
}

void TcpServer::print(QString string)
{
    QTextCodec *codec = QTextCodec::codecForName("CP866");
    QByteArray encodedString = codec->fromUnicode(string);
    std::cout << encodedString.data();
}

int TcpServer::commandRevoke(QString command)
{
    QString comment;
    QStringList commands = command.split(' ', QString::SkipEmptyParts);

    if (commands.size() < 3)
        return 1;

    unsigned serialNo;
    bool b;
    serialNo = commands[1].toInt(&b, 16);

    if ((commands[0] != "revoke") && (!b))
        return 1;

    if (commands.size() == 3)
        comment = commands[2];
    else
    {
        if (commands[2][0] != '"')
            return 1;
        if (command.count('"') != 2)
            return 1;
        comment = command.section('"', 1, 1);
    }

    QString strF = "SELECT * FROM issuedCertificates "
                   "WHERE serialNo=%1";
    strF = strF.arg(serialNo);
    QSqlQuery query;
    if (!query.exec(strF))
        return 1;

    QSqlRecord rec = query.record();
    query.next();

    if (query.isNull("serialNo"))
    {
        print("Неправильный серийный номер\n");
    }

    strF = "SELECT * FROM revokedCertificates "
                   "WHERE serialNo=%1";
    strF = strF.arg(serialNo);
    if (!query.exec(strF))
        return 1;

    rec = query.record();
    query.next();

    if (!(query.isNull("serialNo")))
    {
        print("Сертификат уже отозван\n");
    }

    else
    {
        //comment.chop(1);
        strF = "INSERT INTO  revokedCertificates (serialNo, date, comment) "
               "VALUES(%1, '%2', '%3');";
        strF = strF.arg(serialNo)
                .arg((QString) QDateTime::currentDateTime().toString("yyyy-MM-dd_hh:mm:ss"))
                .arg(comment);
        if (!query.exec(strF)) {
            qDebug() << "Unable to do insert opeation";
        }
        else
            print("Сертификат отозван\n");
    }

    return 0;

}

int TcpServer::commandSelect(QString table)
{
    QString strF = "SELECT * FROM ";

    if (table == "issued")
    {
        strF.append("issuedCertificates ");
        QSqlQuery query;
        if (!query.exec(strF))
            return 1;

        QSqlRecord rec = query.record();

        QStringList k;
        //k << "serialNo" << "subjectDN" << "NotAfter" << "NotBefore";
                                               //2017-06-18_20:54:54
        k << "№\t" << "Действителен с      " << "Действителен до     " << "Субъект";

        for(int i = 0; i < k.length(); i++)
            print(k[i]);
        print("\n\n");
        while (query.next())
        {
            print(query.value(0).toString() + '\t');
            print(query.value(2).toString() + ' ');
            print(query.value(3).toString() + ' ');
            print(query.value(1).toString() + '\n');
        }
        return 0;
    }
    if (table == "revoked")
    {
        strF.append("revokedCertificates ");
        QSqlQuery query;
        if (!query.exec(strF))
            return 1;

        QSqlRecord rec = query.record();

        QStringList k;
        //k << "serialNo" << "date" << "comment";
                     //2017-06-18_20:54:54
        k << "№\t" << "Дата аннулирования  " << "Причина аннулирования\n\n";

        for(int i = 0; i < k.length(); i++)
            print(k[i]);

        while (query.next())
        {
            print(query.value(0).toString() + '\t');
            print(query.value(1).toString() + ' ');
            print(query.value(2).toString() +'\n');
        }
        return 0;
    }

    return 1;
}

void TcpServer::readCommand()
{
    QString command;
    while (1)
    {
        command = cin.readLine();
        QString comment;
        QStringList commands = command.split(' ', QString::SkipEmptyParts);

        if (commands.length() > 0)
        {
            if (commands[0] == "select")
            {
                if(commandSelect(commands[1]))
                    continue;
            }
            if (commands[0] == "revoke")
            {
                if(commandRevoke(command))
                    continue;
            }
            if (commands[0] == "help")
                print("Локальные команды:\n"
                  "  revoke <серийный №> <причина> - аннулировать сертификат\n"
                  "  select <issued/revoked> - вывести на экран реестр выданных (issued) или аннулированных (revoked) сертификатов\n");
        }


    }

}

/*virtual*/ void TcpServer::slotNewConnection()
{
    //qDebug() << "New Connection\n";
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    connect(pClientSocket, SIGNAL(disconnected()),
            pClientSocket, SLOT(deleteLater())
           );
    connect(pClientSocket, SIGNAL(readyRead()),
            this,          SLOT(slotReadClient())
           );

}

void TcpServer::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_4_2);
    QByteArray data;
    for (;;) {
        if (!m_nNextBlockSize) {
            if (pClientSocket->bytesAvailable() < sizeof(quint16)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (pClientSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }
        m_nNextBlockSize = 0;


        in >> data;

    }

    switch ((unsigned char)data[0])
    {
    case 0x1: // запрос на выдачу сертификата
    {
        data.remove(0, 1);
        QSqlQuery query;
        if (!query.exec("SELECT MAX(serialNo) AS max FROM issuedCertificates;")) {
            qDebug() << "Unable to execute query - exiting";
        }
        QSqlRecord rec = query.record();
        query.next();

        X509V3Cert certClient;
        certClient.SerialNum = query.value(rec.indexOf("max")).toInt() + 1;
        if (certClient.SerialNum == 1) certClient.SerialNum = 2;
        certClient.IssuerDN = cert.SubjectDN;

        NiederCrypt crypt;
        crypt.genKey(param_n, param_k);

        data = certClient.getCert(crypt.getPubKey(), data, openFile(KeySelfSignedCertName));

        saveToFile((pathKey + "/%1.key").arg(certClient.SerialNum), certClient.KeyToByte(crypt.getPrivKey()));
        saveToFile((pathCert + "/%1.der").arg(certClient.SerialNum), data);


        // добавление информармации о сертификате в БД

        QString strF =
                "INSERT INTO  issuedCertificates (serialNo, subjectDN, NotAfter, NotBefore) "
                "VALUES(%1, '%2', '%3', '%4');";

        QString subject;
        QMapIterator<QByteArray, QByteArray> iter(certClient.SubjectDN);
        while (1)
        {
            iter.next();
            subject += iter.key() + "=";
            subject += iter.value();
            if (iter.hasNext())
                subject += ", ";
            else break;
        }

        strF = strF.arg(certClient.SerialNum)
                .arg(subject)
                .arg((QString) certClient.NotAfter.toString("yyyy-MM-dd_hh:mm:ss"))
                .arg((QString) certClient.NotBefore.toString("yyyy-MM-dd_hh:mm:ss"));
        if (!query.exec(strF)) {
            qDebug() << "Unable to do insert opeation";
        }

        data = "success";
        break;
    }
    case 0x2:
    {
        data.remove(0, 1);
        //data = openFile(SelfSignedCertName);
        X509V3Cert certClient;
        if (certClient.openCert(data) != 0)
        {
            data = "err";
            break;
        }
        switch (certClient.checkCert(data, cert.PubKey))
        {
        case 0:
        {
            QDateTime dateToday = QDateTime::currentDateTime();
            if ((certClient.NotAfter < dateToday) && (certClient.NotBefore >= dateToday))
            {
                QString strF = "SELECT serialNo FROM revokedCertificates "
                               "WHERE serialNo=%1";
                strF = strF.arg(certClient.SerialNum);
                QSqlQuery query;
                if (!query.exec(strF)) {
                    qDebug() << "Unable to execute query - exiting";
                    data = "err";
                }
                else
                {
                    query.record();
                    query.next();

                    if (query.isNull("serialNo"))
                        data = "valid";
                    else
                        data = "revoked";
                }
            }
            else
                data = "expired"; // срок действия истек или не наступил
            break;
        }
        case 1: //целостность сертификата нарушена
            data = "notValid";
            break;
        default:
            data = "err";
        }

        break;
    }
    default:
        data = "err";
        break;
    }

    //ответ клиенту
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_2);
    out << quint16(0) << data;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    pClientSocket->write(arrBlock);

}

void TcpServer::writeDefaultParam()
{
    QSettings sett("serverSetting.ini", QSettings::IniFormat);
    sett.setValue("port", "2323");
    sett.setValue("PATH/selfSignedCert", "selfSignedCert.der");
    sett.setValue("PATH/privKeyCA", "privKeySelfSignedCert.key");
    sett.setValue("PATH/clientCerts", "clientCerts");
    sett.setValue("PATH/clientKeys", "clientKeys");
    sett.setValue("keyParam/n", "20");
    sett.setValue("keyParam/k", "16");
    sett.setValue("BD/path", "CA.db3");
    sett.setValue("BD/userName", "user");
    sett.setValue("BD/hostName", "host");
    sett.setValue("BD/passwd", "password");

}

bool TcpServer::createConnectionBD()
{
    if (!QFile("serverSetting.ini").exists())
        writeDefaultParam();

    QSettings sett("serverSetting.ini", QSettings::IniFormat);
    SelfSignedCertName = sett.value("PATH/selfSignedCert").toString();
    KeySelfSignedCertName = sett.value("PATH/privKeyCA").toString();

    param_k = sett.value("keyParam/k").toUInt();
    param_n = sett.value("keyParam/n").toUInt();

    pathCert = sett.value("PATH/clientCerts").toString();
    pathKey = sett.value("PATH/clientKeys").toString();
    QDir().mkpath(pathCert);
    QDir().mkpath(pathKey);

    nPort = sett.value("port").toInt();

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(sett.value("BD/path").toString());

    db.setUserName(sett.value("BD/userName").toString());
    db.setHostName(sett.value("BD/hostName").toString());
    db.setPassword(sett.value("BD/passwd").toString());
    if (!db.open())
        return false;

    return true;
}

int TcpServer::saveToFile(QString &path, QByteArray &data)
{
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
        return 1;
    QDataStream outf(&file);
    for(int i = 0; i< data.size(); i++)
        outf << (unsigned char)data[i];
    if(outf.status() != QDataStream::Ok)
    {
        qDebug() << "Ошибка записи файла " << path;
        return 1;
    }
    file.close();
    return 0;
}

QByteArray TcpServer::openFile(QString &path)
{
    QByteArray data;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Ошибка чтения файла " << path;
        return data;
    }
    data = file.readAll();
    file.close();
    return data;
}
