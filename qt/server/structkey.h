#ifndef STRUCTKEY_H
#define STRUCTKEY_H
#include <QtCore>

struct PublicKey {
    QByteArray H;
    unsigned __int16 t;
};

struct PrivateKey {
    QByteArray S, H, P;
};
#endif // STRUCTKEY_H
