#include <niedercrypt.h>

// конструктор класса
NiederCrypt::NiederCrypt()
{
    for(int tmp = p_x; tmp != 1; tmp = tmp >> 1)
        st++;
    Q = 1 << st;
    alpha_of = new unsigned __int8[Q];
    index_of = new unsigned __int8[Q];
    gen_table();
    srand( time(0) );
}

// получить открытый ключ
PublicKey NiederCrypt::getPubKey()
{
    PublicKey key;
    for(int i = 0; i < Hpub.size(); i++)
        for(int k = 0; k < Hpub[0].size(); k++)
            key.H.push_back(Hpub[i][k]);
    key.t = t;

    return key;
}

// получить закрытый ключ
PrivateKey NiederCrypt::getPrivKey()
{
    PrivateKey key;
    for(int i = 0; i < S.size(); i++)
        for(int k = 0; k < S[0].size(); k++)
            key.S.push_back(S[i][k]);

    for(int i = 0; i < H.size(); i++)
        for(int k = 0; k < H[0].size(); k++)
            key.H.push_back(H[i][k]);

    for(int i = 0; i < P.size(); i++)
        for(int k = 0; k < P[0].size(); k++)
            key.P.push_back(P[i][k]);

    return key;
}

void NiederCrypt::setPubKey(PublicKey &PubKey)
{
    t = PubKey.t;
    N = PubKey.H.size() / (t*2);
    K = N - 2*t;
    d_min = N - K + 1;

    Hpub.clear();
    Hpub.resize(N - K);
    for(int i = 0; i < Hpub.size(); i++)
    {
        Hpub[i].resize(N);
        for(int j = 0; j < Hpub[0].size(); j++)
            Hpub[i][j] = PubKey.H[i*N + j];
    }
    return;
}

void NiederCrypt::setPrivKey(PrivateKey &PrivKey)
{
    N = sqrt(PrivKey.P.size());
    K = N - PrivKey.H.size() / N ;
    d_min = N - K + 1;
    t = (d_min - 1) / 2;

    S.clear();
    S.resize(N - K);
    for(int i = 0; i < N - K; i++)
    {
        S[i].resize(N - K);
        for(int j = 0; j < N - K; j++)
            S[i][j] = PrivKey.S[i*(N - K) + j];
    }

    H.clear();
    H.resize(N - K);
    for(int i = 0; i < N - K; i++)
    {
        H[i].resize(N);
        for(int j = 0; j < N; j++)
            H[i][j] = PrivKey.H[i*N + j];
    }

    P.clear();
    P.resize(N);
    for(int i = 0; i < N; i++)
    {
        P[i].resize(N);
        for(int j = 0; j < N; j++)
            P[i][j] = PrivKey.P[i*N + j];
    }

    Hpub = H;//matrix_mul(matrix_mul(S, H), matrix_Transp(P));

    return;
}

// генерация открытого ключа
void NiederCrypt::genKey(unsigned __int8 n, unsigned __int8 k)
{
    N = n;
    K = k;
    d_min = N - K + 1;
    t = (d_min - 1) / 2;

    H.resize(N - K); // проверочная матрица
    for (int i = 0; i < H.size(); i++)
    {
        H[i].resize(N);
        H[i][0] = 1;
        for (int j = 1; j < N; j++)
            H[i][j] = gf_pow(2, j*(i + 1));
    }
    gen_S();
    gen_P();
    Hpub = H;//matrix_mul(matrix_mul(S, H), P);
    P = matrix_Transp(P);

}

// генерация таблицы быстрого умножения в поле Галуа
void NiederCrypt::gen_table()
{
    for (unsigned __int16 i = 0; i < Q; i++)
    {
        if (i < st)
        {
            alpha_of[1 << i] = i;
            index_of[i] = 1 << i;
        }
        else
        {
            unsigned __int16 k = 0, index = 1;
            while (k != i)
            {
                while ((k != i) && (index < Q))
                {
                    index = index << 1;
                    k++;
                }
                if (index >= Q)
                    index ^= p_x;
            }
            alpha_of[index] = i;
            index_of[i] = index;
        }
    }

}

// умножение в поле Галуа
unsigned __int8 NiederCrypt::gf_mul(__int16 a, __int16 b)
{
    if ((a == 0) || (b == 0))
        return 0;
    if (a < 0) a = -a;
    if (b < 0) b = -b;

    return index_of[(alpha_of[a] + alpha_of[b]) % (Q - 1)];
}

// деление в поле Галуа
unsigned __int8 NiederCrypt::gf_div(__int16 a, __int16 b)
{
    if ((a == 0) || (b == 0))
        return 0;
    if (a < 0) a = -a;
    if (b < 0) b = -b;

    __int16 j = alpha_of[a] - alpha_of[b];
    while (j < 0)
        j += Q - 1;

    return index_of[j];
}

// Возведение в степень в поле Галуа
unsigned __int8 NiederCrypt::gf_pow(unsigned __int8 a, __int16 b)
{
    b %= N;
    if (b == 0)
        return 1;
    if (b == 1)
        return a;

    unsigned __int8 r = a;
    for (__int16 i = 1; i < b; i++)
        r = gf_mul(r, a);

    return r;
}

// Транспонирование матрицы
QVector<QVector<unsigned __int8>> NiederCrypt::matrix_Transp(QVector<QVector<unsigned __int8>> &a)
{
    QVector<QVector<unsigned __int8>> r(a[0].size());

    for (int i = 0; i < r.size(); i++)
    {
        r[i].resize(a.size());
        for (int j = 0; j < r[0].size(); j++)
            r[i][j] = a[j][i];
    }

    return r;
}

// умножение полиномов в поле Галуа
QVector<unsigned __int8> NiederCrypt::vec_mul(QVector<unsigned __int8> &a, QVector<unsigned __int8> &b)
{
    QVector<unsigned __int8> r(a.size() + b.size() - 1);

    for (int i = a.size() - 1; i >= 0; i--)
        for (int k = b.size() - 1; k >= 0; k--)
            r[i + k] ^= gf_mul(a[i], b[k]);

    return r;
}

// умножение матриц в поле Галуа
QVector<QVector<unsigned __int8>> NiederCrypt::matrix_mul(QVector<QVector<unsigned __int8>> &a, QVector<QVector<unsigned __int8>> &b)
{
    if (a[0].size() != b.size())
    {
        QVector<QVector<unsigned __int8>> err;
        return err;
    }

    QVector<QVector<unsigned __int8>> r(a.size());


    for (int row = 0; row < a.size(); row++)
    {
        r[row].resize(b[0].size());
        for (int col = 0; col < b[0].size(); col++)
            for (int inner = 0; inner < b.size(); inner++)
                r[row][col] ^= gf_mul(a[row][inner], b[inner][col]);
    }

    return r;
}

// умножение вектора на матрицу в поле Галуа
QVector<unsigned __int8> NiederCrypt::matrix_mul(QVector<unsigned __int8> &a, QVector<QVector<unsigned __int8>> &b)
{
    if (a.size() != b[0].size())
    {
        QVector<unsigned __int8> err;
        return err;
    }

    QVector<unsigned __int8> r(b[0].size());


    for (int col = 0; col < b[0].size(); col++)
        for (int inner = 0; inner < b.size(); inner++)
            r[col] ^= gf_mul(a[inner], b[inner][col]);

    return r;
}

// умножение матрицы на вектор в поле Галуа
QVector<unsigned __int8> NiederCrypt::matrix_mul(QVector<QVector<unsigned __int8>> &a, QVector<unsigned __int8> &b)
{
    if (a[0].size() != b.size())
    {
        QVector<unsigned __int8> err;
        return err;
    }

    QVector<unsigned __int8> r(a.size());


    for (int row = 0; row < a.size(); row++)
        for (int inner = 0; inner < b.size(); inner++)
            r[row] ^= gf_mul(a[row][inner], b[inner]);

    return r;
}

// умножение матрицы на вектор в поле Галуа
QVector<unsigned __int8> NiederCrypt::matrix_mul(QVector<QVector<unsigned __int8>> &a, QByteArray &b)
{
    if (a[0].size() != b.size())
    {
        QVector<unsigned __int8> err;
        return err;
    }

    QVector<unsigned __int8> r(a.size());


    for (int row = 0; row < a.size(); row++)
        for (int inner = 0; inner < b.size(); inner++)
            r[row] ^= gf_mul(a[row][inner], (unsigned char)b[inner]);

    return r;
}

//функция вычеркивания строки и столбца
QVector<QVector<unsigned __int8>> NiederCrypt::Get_matr(QVector<QVector<unsigned __int8>> &Matr, int indRow, int indCol)
{
    int n = Matr.size(), ki = 0;
    QVector<QVector<unsigned __int8>> temp_matr(n - 1);

    for (int i = 0; i < n; i++)
    {
        if (i != indRow)
        {
            temp_matr[ki].resize(n - 1);
            for (int j = 0, kj = 0; j < n; j++)
                if (j != indCol)
                {
                    temp_matr[ki][kj] = Matr[i][j];
                    kj++;
                }
            ki++;
        }
    }
    return temp_matr;
}

// нахождение определителя
unsigned __int8 NiederCrypt::Det(QVector<QVector<unsigned __int8>> &matr)
{
    int l, n = matr.size();
    unsigned __int8 sum11 = 1, sum12 = 0, sum21 = 1, sum22 = 0;

    if (n == 1)
        return matr[0][0];
    else if (n == 2)
        return gf_mul(matr[0][0], matr[1][1]) ^ gf_mul(matr[1][0], matr[0][1]);
    else
    {
        for (int i = 0; i < n; i++)
        {
            sum11 = 1;
            l = 2 * n - 1 - i;
            sum21 = 1;
            for (int j = 0; j < n; j++)
            {
                sum21 = gf_mul(sum21, matr[j][l % n]);
                l--;
                sum11 = gf_mul(sum11, matr[j][(j + i) % n]);
            }
            sum22 ^= sum21;
            sum12 ^= sum11;
        }
        return (sum12 ^ sum22);
    }
}

// нахождение обратной матрицы
QVector<QVector<unsigned __int8>> NiederCrypt::Obr(QVector<QVector<unsigned __int8>> &Matrix)
{
    unsigned __int8 det = Det(Matrix);
    int n = Matrix.size();
    QVector<QVector<unsigned __int8>> obr_matr(n);

    if (det != 0)
    {
        for (int i = 0; i < n; i++)
        {
            obr_matr[i].resize(n);
            for (int j = 0; j < n; j++)
            {
                obr_matr[i][j] = gf_div(Det(Get_matr(Matrix, i, j)), det);
            }
        }
    }
    return matrix_Transp(obr_matr);
}

// генерация матрицы S
void NiederCrypt::gen_S()
{
    unsigned __int8 det = 0;
    S.resize(N - K);
    while (det == 0)
    {
        // заполнение матрицы
        for (int row = 0; row < N - K; row++)
        {
            S[row].resize(N - K);
            for (int col = 0; col < N - K; col++)
                S[row][col] = (unsigned char) (rand() % Q); // random
        }

        // Проверка: нахождение детерминанта
        det = Det(S);
    }
}

// генерация матрицы перестановки P
void NiederCrypt::gen_P()
{
    P.resize(N);
    QVector<unsigned __int8> Rand(N);

    for (int i = 0; i < N; i++)
        Rand[i] = i;
    std::random_shuffle(Rand.begin(), Rand.end());

    for (int i = 0; i < N; i++)
    {
        P[i].resize(N);
        P[i][Rand[i]] = 1;
    }
}

unsigned __int8 NiederCrypt::f_x(QVector<unsigned __int8> &f, unsigned __int8 x)
{
    unsigned __int8 sum = 0;
    for (int i = 0; i < f.size(); i++)
        sum ^= gf_mul(f[i], gf_pow(x, i));
    return sum;
}

// декодирование
QVector<unsigned char> NiederCrypt::decoder(QByteArray &syndr)
{
    //QVector<unsigned __int8> s1(N - K);
    //s1[5] = 9; s1[4] = 3; s1[3]=2;s1[2]=15;s1[1]=15;
    //s1[3] = 0; s1[2] = 1; s1[1]= 3; s1[0]= 6;

    QVector<unsigned char> E, s1;
    //s1 << 153 << 132 << 108 << 253;
    //s1 = matrix_mul(S, s1);

    for(int i = 0; i < syndr.size(); i++)
        s1.push_back(syndr[i]);
    //s1 = matrix_mul(S, syndr);
    for(int v = t; v > 0; v--)
    {
        E.clear();
        QVector<QVector<unsigned __int8>> M(v);

        for (int i = 0; i < v; i++)
        {
            M[i].resize(v);
            for (int j = 0; j < v; j++)
                M[i][j] = s1[v - 1 - j + i];
        }
        if (Det(M) == 0)
            continue;

        M = Obr(M);
        QVector<unsigned __int8> L, V(v);
        for (int i = v; i < 2 * v; i++)
            V[i - v] = s1[i];


        L = matrix_mul(M, V); // получаем коэффициенты многочлена локаторов ошибок
        M.clear();
        L.insert(L.begin(), 1);

        QVector<unsigned __int8>
                X, // корни многочлена локаторов ошибок
                Y; // позиция ошибок

        for (unsigned __int8 a = 1, i = 0; i < N; a = gf_mul(2, a), i++) // процедура Ченя
        {
            unsigned __int8 b = gf_div(1, a);
            if (f_x(L, b) == 0)
            {
                X.push_back(b);
                Y.push_back(i);
                if (X.size() == v) break;
            }
        }

        if (X.size() != v)
            continue; // синдром недекадируемый

        QVector<unsigned __int8> W(v);
        for (int i = 0; i < v; i++)
            for (int j = 0; j <= i; j++)
                W[i] ^= gf_mul(L[j], (unsigned char)s1[i - j]);

        QVector<unsigned __int8> L_der(L.size() - 1);
        for (int i = 0; i < L_der.size(); i += 2) // нахождение производной
            L_der[i] = L[i + 1];

        E.resize(N);
        for (int i = 0; i < v; i++) // алгоритм Форни с производной
            E[Y[i]] = gf_div(f_x(W, X[i]), f_x(L_der, X[i]));
        // E -- результат декодирования синдрома

        //E = matrix_mul(P, E);
        if (matrix_mul(Hpub, E) != s1)
            continue;

        return E;
    }
    E.clear();
    return E;
}

// вычисление хэша
QByteArray NiederCrypt::hash(QByteArray &D)
{
    QCryptographicHash hsh(Algorithm);
    hsh.addData(D);
    QByteArray rez = hsh.result();
    rez.resize(N - K);
    for(int i = 0; i < rez.size(); i++)
        rez[i] = (char)((unsigned)rez[i] % Q) ;
    return rez;

}

QVector<unsigned char> NiederCrypt::callDecoder (QByteArray & syndr, unsigned i)
{
    QByteArray syndr_i;
    QDataStream syndrStream(&syndr_i, QIODevice::WriteOnly);
    syndrStream << i;
    syndr_i = syndr + syndr_i;
    syndr_i = hash(syndr_i);
    return decoder(syndr_i);
}

// подпись
QByteArray NiederCrypt::signature(QByteArray &D)
{
    QByteArray syndr;
    syndr = hash(D);
    unsigned i = 0;
    while (1)
    {
        QVector<QFuture<QVector<unsigned char>>> futures(QThread::idealThreadCount());

        for(unsigned k = 0; k < futures.size(); k++)
        {
            futures[k] = QtConcurrent::run(this, &NiederCrypt::callDecoder, syndr, i + k);
        }

        for(unsigned k = 0; k < futures.size(); k++)
        {
            QVector<unsigned char> z = futures[k].result();
            if (z.size() == N)
            {
                syndr.clear();
                QDataStream zStream(&syndr, QIODevice::WriteOnly);
                for(int j = 0; j < z.size(); j++)
                    zStream << z[j];

                zStream << i + k;

                return syndr;
            }
        }
        i += QThread::idealThreadCount();
    }
}

bool NiederCrypt::checkSign(QByteArray &D, QByteArray &sign)
{
    QByteArray s2 = sign;
    QVector<unsigned char> z;
    for(int i = 0; i < (N); i++)
        z.push_back(s2[i]);

    s2.remove(0, N);
    s2 = hash(D) + s2;
    s2 = hash(s2);

    QByteArray s1;
    z = matrix_mul(Hpub, z);
    for(int i = 0; i < z.size(); i++)
        s1.push_back(z[i]);



    if (s1 != s2)
        return false;
    return true;
}
