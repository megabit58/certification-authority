#include <QCoreApplication>
#include "niedercrypt.h"
#include "x509v3cert.h"
#include "structkey.h"
#include "TcpServer.h"
#include <QtDebug>

#include "QtSql/QSqlDatabase"
#include "QSqlRecord"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TcpServer Server ;
    Server.startServer();

    return a.exec();
}
