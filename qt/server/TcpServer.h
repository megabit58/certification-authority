#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QtDebug>

#include <x509v3cert.h>
#include "QtSql/QSqlDatabase"
#include "QSqlRecord"
#include <QtConcurrentRun>

class TcpServer : public QObject
{
    Q_OBJECT
private:
    QTcpServer* m_ptcpServer;
    quint16     m_nNextBlockSize;
    int nPort;
    X509V3Cert cert;
    QString SelfSignedCertName, KeySelfSignedCertName, pathKey, pathCert;
    unsigned char param_k, param_n;

private:
    void writeDefaultParam(); // записывает параметры по умолчанию в ini файл
    bool createConnectionBD(); // соединение с БД
    int saveToFile(QString &, QByteArray &); // сохранение в файл
    QByteArray openFile(QString &);
    void readCommand(); // чтение и обработка данных из сокета
    void print(QString string);
    int commandRevoke(QString command);
    int commandSelect(QString table);

public:
    TcpServer(QObject* pwgt = 0);
    int startServer();

public slots:
    virtual void slotNewConnection();
            void slotReadClient   ();
};

#endif // TCPSERVER_H
