#ifndef X509V3CERT_H
#define X509V3CERT_H
#include "structKey.h"
#include "niedercrypt.h"
#include <QtCore>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"



class X509V3Cert
{

public:
    unsigned __int32    SerialNum;          // серийный номер
    PublicKey   PubKey;               // открытый ключ
    QMap<QByteArray, QByteArray>
        SubjectDN,         // данные о субъекте
        IssuerDN;          // данные о выпускающем

    QByteArray
        SignatureAlgorithm = "",// алгоритм подписи
        Signature = "";         // подпись
    QDateTime
        NotAfter,           // недействителен после
        NotBefore;          // недействителен до

private:
    const QMap<QByteArray, unsigned __int32> X509NameConst = {
        { "C",  0x550406 },
        { "ST", 0x550408 },
        { "L",  0x550407 },
        { "O",  0x55040A },
        { "OU", 0x55040B },
        { "CN", 0x550403 }
    };
    const QMap<unsigned __int32, QByteArray> revX509NameConst = {
        { 0x550406, "C" },
        { 0x550408, "ST"},
        { 0x550407, "L"},
        { 0x55040A, "O"},
        { 0x55040B, "OU"},
        { 0x550403, "CN"}
    };

public:
    QByteArray X509NameToDer(QMap<QByteArray, QByteArray> &Name); // кодирование данных об издателе и субъекте
    QByteArray getCert(PublicKey &pubKey, QByteArray &req, QByteArray &prKey); // обработка запроса на выдачу сертификата
    QByteArray getSelfSignedCert(QByteArray &, QByteArray &); // возвращает самоподписанный сертификат
    int checkCert(QByteArray &, PublicKey &); // проверка сертификата
    QByteArray KeyToByte(PublicKey key); // кодирование ключей
    QByteArray KeyToByte(PrivateKey key); //                    в DER
    int openCert(QByteArray &); // декодирование цифрового сертификата

private:
    void addSignInfo(QByteArray &vec);
    unsigned char getSizeInt(unsigned __int32); // узнать количество занятых байт числом
    void sizeToDer(QByteArray &, unsigned __int32); // записать размер в вектор
    void intToDer(QByteArray &, unsigned __int32); // записать число в вектор
    void ByteToByte(QByteArray &, unsigned __int32, unsigned char); // записать int побайтно в массив
    unsigned int sizeDecoder(QByteArray &); // декодирование размера поля

    QByteArray genTSB(QByteArray &, int); // генерация TSB
    unsigned __int32 derToInt(QByteArray &); // декодирование int из массива данных
    QByteArray getData(QByteArray &); // возвращает данные из блока
    QMap<QByteArray, QByteArray> DerToX509Name(QByteArray &); // декодирование данных об издателе и субъекте
    PrivateKey keyDecoder(QByteArray &); // декодирование
    PublicKey pubKeyDecoder(QByteArray &); //             ключей

};
#endif // X509V3CERT_H
