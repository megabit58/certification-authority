#include <x509v3cert.h>

#define SEQUENCE (unsigned char)0x30
#define INTEGER (unsigned char)0x02
#define OBJ_ID (unsigned char)0x06
#define UTCTime (unsigned char)0x17
#define GenTime (unsigned char)0x18
#define SET (unsigned char)0x31
#define UTF8String (unsigned char)0x0C
#define PrintString (unsigned char)0x13
#define NULLder (unsigned char)0x05
#define BIT_STR (unsigned char)0x03

void X509V3Cert::addSignInfo(QByteArray &vec)
{
    unsigned char a[] = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x05, 0x05, 0x00 };
    for (int i = 0; i < 15; i++)
        vec.push_back(a[i]);
}

QByteArray X509V3Cert::X509NameToDer(QMap<QByteArray, QByteArray> &Name)
{
    QByteArray vec;

    vec.push_back(SEQUENCE);
    QByteArray tmp;
    QMapIterator<QByteArray, QByteArray> i(Name);
    while (i.hasNext())
    {
        i.next();
        int size = i.value().size();
        if (size > 127)
            return NULL;

        tmp.push_back(SET);
        QByteArray tmp2;
        {
            tmp2.push_back(SEQUENCE);
            QByteArray tmp3;
            {
                //obj:
                tmp3.push_back(OBJ_ID);
                intToDer(tmp3, X509NameConst[i.key()]);

                if (i.key() == "C")
                    tmp3.push_back(PrintString);
                else
                    tmp3.push_back(UTF8String);
                tmp3.push_back((unsigned char)size);
                for (int k = 0; k < size; k++)
                    tmp3.push_back(i.value()[k]);
            }
            sizeToDer(tmp2, tmp3.size());
            tmp2.append(tmp3);
        }
        sizeToDer(tmp, tmp2.size());
        tmp.append(tmp2);
    }
    sizeToDer(vec, tmp.size());
    vec.append(tmp);

    return vec;
}

unsigned __int32 X509V3Cert::derToInt(QByteArray &arr)
{
    QByteArray data = getData(arr);
    unsigned __int32 m = 0;
    for(int i = 0; i < data.size(); i++)
        m |= (unsigned char)data[i] << (8*(data.size() - 1 - i));
    return m;
}

QByteArray X509V3Cert::getData(QByteArray &arr)
{
    QByteArray data;
    unsigned int size = sizeDecoder(arr);
    if (size > 127)
        arr.remove(2, (unsigned char)arr[1] ^ (unsigned char)0x80);

    arr.remove(0, 2);
    data = arr.left(size);
    arr.remove(0, size);
    return data;
}

QMap<QByteArray, QByteArray> X509V3Cert::DerToX509Name(QByteArray &name)
{
    QMap<QByteArray, QByteArray> r;
    while(name.size() > 0)
    {
        if ((unsigned char)name[0] == SET)
        {
            QByteArray set = getData(name);
            if ((unsigned char)set[0] == SEQUENCE)
            {
                QByteArray seq = getData(set);
                QByteArray key;
                if ((unsigned char)seq[0] == OBJ_ID)
                {
                    key = revX509NameConst[derToInt(seq)];
                }

                if (((unsigned char)seq[0] == UTF8String) || ((unsigned char)seq[0] == PrintString))
                    r[key] = getData(seq);
                //qDebug() << key << ":" << r[key];
            }
        }

    }
    return r;
}

unsigned char X509V3Cert::getSizeInt(unsigned __int32 n)
{
    unsigned __int32 mask = 0xFF000000;
    unsigned char c = 4;
    if (n == 0)
        return 1;
    while (1)
    {
        if ((mask & n) == 0)
        {
            c--;
            mask = mask >> 8;
        }
        else
            break;
    }
    return c;
}

void X509V3Cert::sizeToDer(QByteArray &vec, unsigned __int32 n)
{
    if (n < 127)
        vec.push_back(n);
    else
    {
        //unsigned char countDop = count / 256 + 1;
        unsigned char countDop = getSizeInt(n);
        vec.push_back(countDop | (unsigned char)0x80);
        ByteToByte(vec, n, countDop);
    }
}

void X509V3Cert::ByteToByte(QByteArray &vec, unsigned __int32 n, unsigned char count)
{
    unsigned __int32 mask = 0xFF000000 >> (32 - count * 8);
    for (count; count > 0; count--)
    {
        vec.push_back((n & mask) >> ((count - 1) * 8));
        mask = mask >> 8;
    }
}

void X509V3Cert::intToDer(QByteArray &vec, unsigned __int32 n)
{
    unsigned char count = getSizeInt(n);
    sizeToDer(vec, count);
    ByteToByte(vec, n, count);
}

QByteArray X509V3Cert::KeyToByte(PublicKey key)
{
    QByteArray vec;
    vec.push_back(SEQUENCE);
    QByteArray tmp3;
    {
        tmp3.push_back(SEQUENCE);
        tmp3.push_back(10 + 3);

        tmp3.push_back(OBJ_ID);
        unsigned char a[] = { 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01 }; // информация о ключе
        for (int i = 0; i < 10; i++)
            tmp3.push_back(a[i]);
        tmp3.push_back(NULLder);
        tmp3.push_back((char)0);

        tmp3.push_back(BIT_STR);
        QByteArray tmp2;
        {
            tmp2.push_back(SEQUENCE);

            QByteArray tmp;
            {
                tmp.push_back(INTEGER);
                sizeToDer(tmp, key.H.size() + 1); // размер ключа
                tmp.push_back((char)0);
                tmp.append(key.H);

                tmp.push_back(INTEGER);
                intToDer(tmp, key.t);
            }
            sizeToDer(tmp2, tmp.size());
            tmp2.append(tmp);
        }
        sizeToDer(tmp3, tmp2.size() + 1);
        tmp3.push_back((char)0);
        tmp3.append(tmp2);
    }
    sizeToDer(vec, tmp3.size());
    vec.append(tmp3);

    return vec;
}

QByteArray X509V3Cert::KeyToByte(PrivateKey key)
{
    QByteArray vec;
    vec.push_back(SEQUENCE);
    QByteArray tmp3;
    {
        tmp3.push_back(SEQUENCE);
        tmp3.push_back(10 + 3);

        tmp3.push_back(OBJ_ID);
        unsigned char a[] = { 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01 }; // информация о ключе
        for (int i = 0; i < 10; i++)
            tmp3.push_back(a[i]);
        tmp3.push_back(NULLder);
        tmp3.push_back((char)0);

        tmp3.push_back(BIT_STR);
        QByteArray tmp2;
        {
            tmp2.push_back(SEQUENCE);

            QByteArray tmp;
            {
                tmp.push_back(INTEGER);
                sizeToDer(tmp, key.S.size()); // размер ключа
                tmp.append(key.S);

                tmp.push_back(INTEGER);
                sizeToDer(tmp, key.H.size()); // размер ключа
                tmp.append(key.H);

                tmp.push_back(INTEGER);
                sizeToDer(tmp, key.P.size()); // размер ключа
                tmp.append(key.P);
            }
            sizeToDer(tmp2, tmp.size());
            tmp2.append(tmp);
        }
        sizeToDer(tmp3, tmp2.size() + 1);
        tmp3.push_back((char)0);
        tmp3.append(tmp2);
    }
    sizeToDer(vec, tmp3.size());
    vec.append(tmp3);

    return vec;
}

unsigned int X509V3Cert::sizeDecoder(QByteArray &arr)
{
    unsigned char count = arr[1];
    if(count > (unsigned char)0x80)
    {
        count ^= 0x80;
        unsigned int size = 0;
        for (int i = 0; i < count; i++)
        {
            size |= (unsigned char)arr[2 + i] << ((count - 1 - i)*8);
        }
        return size;
    }
    else
        return count;
}


PrivateKey X509V3Cert::keyDecoder(QByteArray &key)
{
    PrivateKey r;
    if ((unsigned char)key[0] == SEQUENCE)
    {
        key = getData(key);
        if ((unsigned char)key[0] == SEQUENCE)
        {
            getData(key);
            if((unsigned char)key[0] == BIT_STR)
            {
                key = getData(key);
                key.remove(0, 1);
                if((unsigned char)key[0] == SEQUENCE)
                {
                    key = getData(key);
                    r.S = getData(key);
                    r.H = getData(key);
                    r.P = getData(key);
                }
            }
        }
    }
    return r;
}

PublicKey X509V3Cert::pubKeyDecoder(QByteArray &key)
{
    PublicKey r;
    if ((unsigned char)key[0] == SEQUENCE)
    {
        key = getData(key);
        if ((unsigned char)key[0] == SEQUENCE)
        {
            getData(key);
            if((unsigned char)key[0] == BIT_STR)
            {
                key = getData(key);
                key.remove(0, 1);
                if((unsigned char)key[0] == SEQUENCE)
                {
                    key = getData(key);
                    r.H = getData(key);
                    r.H.remove(0, 1);
                    r.t = derToInt(key);
                }
            }
        }
    }
    return r;
}

QByteArray X509V3Cert::getSelfSignedCert(QByteArray &pKey, QByteArray &prKey) // генерация самоподписанного сертификата
{
    SerialNum = 1;
    NiederCrypt crypt;
    crypt.setPrivKey(keyDecoder(prKey));

    QByteArray cert;

    SubjectDN = IssuerDN;
    QByteArray TSB = genTSB(pKey, 10);
    QByteArray sign = crypt.signature(TSB);

    QByteArray tmp;
    addSignInfo(tmp); // используемый алгоритм ЭЦП
    tmp.push_back(BIT_STR);
    sizeToDer(tmp, sign.size() + 1);
    tmp.push_back((char)0);

    cert.push_back(SEQUENCE);
    sizeToDer(cert, tmp.size() + sign.size() + TSB.size());
    cert.append(TSB);
    cert.append(tmp);
    cert.append(sign);

    return cert;
}

QByteArray X509V3Cert::getCert(PublicKey &pubKey, QByteArray &req, QByteArray &prKey)
{
    QByteArray cert;
    int period = req[0];
    req.remove(0, 1);
    if(req[0] == (char)SEQUENCE)
        SubjectDN = DerToX509Name(getData(req));
    else
        return cert;

    NiederCrypt crypt;
    crypt.setPrivKey(keyDecoder(prKey));

    QByteArray TSB = genTSB(KeyToByte(pubKey), period);
    QByteArray sign = crypt.signature(TSB);

    QByteArray tmp;
    addSignInfo(tmp); // используемый алгоритм ЭЦП
    tmp.push_back(BIT_STR);
    sizeToDer(tmp, sign.size() + 1);
    tmp.push_back((char)0);

    cert.push_back(SEQUENCE);
    sizeToDer(cert, tmp.size() + sign.size() + TSB.size());
    cert.append(TSB);
    cert.append(tmp);
    cert.append(sign);



    return cert;
}

QByteArray X509V3Cert::genTSB(QByteArray &Pkey, int period)
{
    QByteArray TSB;

    TSB.push_back(SEQUENCE);
    QByteArray tmp;
    {

        tmp.push_back(0xA0);
        tmp.push_back(0x03);
        tmp.push_back(INTEGER);
        tmp.push_back(0x01);
        tmp.push_back(0x02);

        tmp.push_back(INTEGER);
        intToDer(tmp, SerialNum); // серийный номер

        addSignInfo(tmp); // используемый алгоритм ЭЦП

        tmp.push_back(X509NameToDer(IssuerDN)); // издатель сертификата

        // добавление срока действия
        NotAfter = QDateTime::currentDateTime();
        tmp.push_back(SEQUENCE);
        tmp.push_back(0x20);
        tmp.push_back(UTCTime);
        tmp.push_back(0x0D);
        tmp.push_back(NotAfter.toString("yyMMddhhmmss").toUtf8());
        tmp.push_back(0x5A);

        tmp.push_back(GenTime);
        tmp.push_back(0x0F);
        NotBefore = NotAfter.addYears(period);
        tmp.push_back(NotBefore.toString("yyyyMMddhhmmss").toUtf8());
        tmp.push_back(0x5A);


        tmp.push_back(X509NameToDer(SubjectDN));

        tmp.push_back(Pkey);
    }
    sizeToDer(TSB, tmp.size());
    TSB.append(tmp);
    return TSB;
}

int X509V3Cert::checkCert(QByteArray &cert, PublicKey &key)
{
    if((unsigned char)cert[0] == SEQUENCE)
    {
         cert = getData(cert);
        if((unsigned char)cert[0] == SEQUENCE)
        {
            unsigned int sizeTSB = sizeDecoder(cert) + 2;
            if (sizeTSB > 129)
                sizeTSB += (unsigned char)cert[1] ^ (unsigned char)0x80;
            QByteArray TSB = cert.left(sizeTSB);
            cert.remove(0, sizeTSB);
            if((unsigned char)cert[0] == SEQUENCE)
            {
                getData(cert);
                if((unsigned char)cert[0] == BIT_STR)
                {
                    QByteArray sign = getData(cert);
                    sign.remove(0, 1);

                    NiederCrypt crypt;
                    crypt.setPubKey(key);

                    if (crypt.checkSign(TSB, sign) == true)
                        return 0;
                    else
                        return 1;
                }
            }
        }
    }
    return 2;
}

int X509V3Cert::openCert(QByteArray &in)
{
    QByteArray cert = in;
    if((unsigned char)cert[0] == SEQUENCE)
    {
         cert = getData(cert);
        if((unsigned char)cert[0] == SEQUENCE)
        {
            QByteArray TSB = getData(cert);
            if ((unsigned char)TSB[0] == 0xA0)
            {
                getData(TSB);
                if ((unsigned char)TSB[0] == INTEGER)
                    SerialNum = derToInt(TSB);
                getData(TSB);
                if((unsigned char)TSB[0] == SEQUENCE)
                    IssuerDN = DerToX509Name(getData(TSB));

                if((unsigned char)TSB[0] == SEQUENCE)
                {
                    QByteArray period = getData(TSB);
                    QString tmp = getData(period);
                    tmp.resize(tmp.size() - 1);
                    NotAfter = QDateTime::fromString(tmp, "yyMMddhhmmss");
                    NotAfter = NotAfter.addYears(100);
                    tmp = getData(period);
                    tmp.resize(tmp.size() - 1);
                    NotBefore = QDateTime::fromString(tmp, "yyyyMMddhhmmss");
                }

                if((unsigned char)TSB[0] == SEQUENCE)
                    SubjectDN = DerToX509Name(getData(TSB));

                if((unsigned char)TSB[0] == SEQUENCE)
                    PubKey = pubKeyDecoder(TSB);
                return 0;
            }
        }

    }
    return 1;
}

